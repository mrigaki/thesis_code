/**
  * Created by marik0 on 17.03.17.
  */
package thesis

import java.io.File
import org.apache.spark.rdd.RDD
import org.apache.spark.graphx._
import scala.util.hashing.MurmurHash3

case class Flow(time: Long, dur: Long, src: String, srcPort: String, dest: String, destPort: String,
                protocol: String, pktCount: Long, byteCount: Long)

object Flows {
  import org.apache.spark.sql.SparkSession


  val spark: SparkSession =
    SparkSession
      .builder()
      .appName("Flows")
      .config("spark.master", "local")
      .getOrCreate()

  // For implicit conversions like converting RDDs to DataFrames
  import spark.implicits._

  //def filePath = new File(this.getClass.getClassLoader.getResource("data/flows.txt").toURI).getPath
  // def filePath = new File("/media/marik0/experiments/data/security/lanl/flows.txt").getPath

  def main(args: Array[String]): Unit = {

    val filePath = args(0).toString

    val flowsDF = spark.sparkContext
      .textFile(filePath)
      .map(_.split(","))
      .map(cols => Flow(cols(0).trim.toLong, cols(1).trim.toLong, cols(2), cols(3), cols(4), cols(5), cols(6), cols(7).trim.toLong, cols(8).trim.toLong))
      .toDF()

    // Add a new column with the end of the connection
    val newDF = flowsDF.withColumn("end", flowsDF("time") + flowsDF("dur"))
    newDF.cache()
    //println(newDF.show(3))

    val fromTo = newDF.select(newDF("src"), newDF("dest"))
    val nodesList = newDF.select(newDF("src"), newDF("dest")).flatMap(x => Iterable(x(0).toString, x(1).toString))
    val vertices:  RDD[(VertexId, String)] = nodesList.distinct().map(x => (MurmurHash3.stringHash(x).toLong, x)).as[(VertexId, String)].rdd
    val uniqueHNodes = vertices.map(_._1).count()

    val edges = fromTo.map(x => ((MurmurHash3.stringHash(x(0).toString).toLong, MurmurHash3.stringHash(x(1).toString).toLong), 1)).map(x => Edge(x._1._1, x._1._2,x._2)).rdd
    val flowGraph = Graph(vertices, edges)
    flowGraph.persist()
    flowGraph.degrees.cache()
    flowGraph.inDegrees.cache()
    flowGraph.outDegrees.cache()
    //println(flowGraph.numVertices, flowGraph.numEdges)
    //println(flowGraph.vertices.count())

    println(flowGraph.outDegrees.join(vertices).sortBy(_._2._1, ascending=false).take(1)(0))
    //val ds = flowsDF.as[Flow]
    //flowsDF.drop("dur")
    //ds.format("csv").write("qout")



  }

}
