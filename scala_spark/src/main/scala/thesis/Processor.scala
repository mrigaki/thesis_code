/**
  * Created by marik0 on 17.03.17.
  */
package thesis

import java.io.File
import org.apache.spark.rdd.RDD
import org.apache.spark.graphx._
import scala.util.hashing.MurmurHash3

case class Flow(time: Long, dur: Long, src: String, srcPort: String, dest: String, destPort: String,
                protocol: String, pktCount: Long, byteCount: Long)
case class ReducedFlow(time: Long, src: String, dest: String, pktCount: Long)

object Flows {
  import org.apache.spark.sql.SparkSession


  val spark: SparkSession =
    SparkSession
      .builder()
      .appName("Flows")
      .config("spark.master", "local")
      .getOrCreate()

  // For implicit conversions like converting RDDs to DataFrames
  import spark.implicits._

  //def filePath = new File(this.getClass.getClassLoader.getResource("data/flows.txt").toURI).getPath
  def filePath = new File("/media/marik0/experiments/data/security/lanl/flows.txt").getPath
  def outFile = new File("/media/marik0/experiments/data/security/lanl/out_flows.txt")

  def main(args: Array[String]): Unit = {

    //val filePath = args(0).toString

    val flowsDF = spark.sparkContext
      .textFile(filePath)
      .map(_.split(","))
      .map(cols => ReducedFlow(cols(0).trim.toLong, cols(2), cols(4), cols(7).trim.toLong)
      .toDF()

    // Add a new column with the end of the connection
    // val newDF = flowsDF.withColumn("end", flowsDF("time") + flowsDF("dur"))
    // newDF.cache()

   val meanPktCount = flowsDF("pktCount").mean


    //val ds = flowsDF.as[Flow]
    //flowsDF.drop("dur")
    //ds.format("csv").write("qout")



  }

}
