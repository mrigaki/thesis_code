name := "thesis_code"

version := "1.0"

scalaVersion := "2.12"
scalacOptions ++= Seq("-deprecation")

libraryDependencies += "org.apache.spark" %% "spark-core" % "2.0.0"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.0.0"
libraryDependencies += "org.apache.spark" %% "spark-graphx" % "2.0.0"