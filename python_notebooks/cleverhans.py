import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder, MinMaxScaler

from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import RMSprop, adam

from cleverhans.attacks import fgsm, jsma
from cleverhans.utils_tf import model_train, model_eval, batch_eval
from cleverhans.attacks_tf import jacobian_graph
from cleverhans.utils import other_classes

import tensorflow as tf
from tensorflow.python.platform import flags

FLAGS = flags.FLAGS

flags.DEFINE_integer('nb_epochs', 5, 'Number of epochs to train model')
flags.DEFINE_integer('batch_size', 128, 'Size of training batches')
flags.DEFINE_float('learning_rate', 0.1, 'Learning rate for training')
flags.DEFINE_integer('nb_classes', 5, 'Number of classification classes')
flags.DEFINE_integer('source_samples', 10, 'Nb of test set examples to attack')


names = ['duration', 'protocol', 'service', 'flag', 'src_bytes', 'dst_bytes', 'land', 'wrong_fragment',
          'urgent', 'hot', 'num_failed_logins', 'logged_in', 'num_compromised', 'root_shell', 'su_attempted',
          'num_root', 'num_file_creations', 'num_shells', 'num_access_files', 'num_outbound_cmds',
          'is_host_login', 'is_guest_login', 'count', 'srv_count', 'serror_rate', 'srv_serror_rate',
          'rerror_rate', 'srv_rerror_rate', 'same_srv_rate', 'diff_srv_rate', 'srv_diff_host_rate',
          'dst_host_count', 'dst_host_srv_count', 'dst_host_same_srv_rate', 'dst_host_diff_srv_rate',
          'dst_host_same_src_port_rate', 'dst_host_srv_diff_host_rate', 'dst_host_serror_rate',
          'dst_host_srv_serror_rate', 'dst_host_rerror_rate', 'dst_host_srv_rerror_rate', 'attack_type', 'other']

df = pd.read_csv('KDDTrain+.txt', names=names, header=None)
dft = pd.read_csv('KDDTest+.txt', names=names, header=None)
print("Initial test and training data shapes:", df.shape[0], dft.shape[0])

full = pd.concat([df, dft])
assert full.shape[0] == df.shape[0] + dft.shape[0]

full['label'] = full['attack_type']

# DoS attacks
full.loc[full.label == 'neptune', 'label'] = 'dos'
full.loc[full.label == 'back', 'label'] = 'dos'
full.loc[full.label == 'land', 'label'] = 'dos'
full.loc[full.label == 'pod', 'label'] = 'dos'
full.loc[full.label == 'smurf', 'label'] = 'dos'
full.loc[full.label == 'teardrop', 'label'] = 'dos'
full.loc[full.label == 'mailbomb', 'label'] = 'dos'
full.loc[full.label == 'processtable', 'label'] = 'dos'
full.loc[full.label == 'udpstorm', 'label'] = 'dos'
full.loc[full.label == 'apache2', 'label'] = 'dos'
full.loc[full.label == 'worm', 'label'] = 'dos'

# User-to-Root (U2R)
full.loc[full.label == 'buffer_overflow', 'label'] = 'u2r'
full.loc[full.label == 'loadmodule', 'label'] = 'u2r'
full.loc[full.label == 'perl', 'label'] = 'u2r'
full.loc[full.label == 'rootkit', 'label'] = 'u2r'
full.loc[full.label == 'sqlattack', 'label'] = 'u2r'
full.loc[full.label == 'xterm', 'label'] = 'u2r'
full.loc[full.label == 'ps', 'label'] = 'u2r'

# Remote-to-Local (R2L)
full.loc[full.label == 'ftp_write', 'label'] = 'r2l'
full.loc[full.label == 'guess_passwd', 'label'] = 'r2l'
full.loc[full.label == 'imap', 'label'] = 'r2l'
full.loc[full.label == 'multihop', 'label'] = 'r2l'
full.loc[full.label == 'phf', 'label'] = 'r2l'
full.loc[full.label == 'spy', 'label'] = 'r2l'
full.loc[full.label == 'warezclient', 'label'] = 'r2l'
full.loc[full.label == 'warezmaster', 'label'] = 'r2l'
full.loc[full.label == 'xlock', 'label'] = 'r2l'
full.loc[full.label == 'xsnoop', 'label'] = 'r2l'
full.loc[full.label == 'snmpgetattack', 'label'] = 'r2l'
full.loc[full.label == 'httptunnel', 'label'] = 'r2l'
full.loc[full.label == 'snmpguess', 'label'] = 'r2l'
full.loc[full.label == 'sendmail', 'label'] = 'r2l'
full.loc[full.label == 'named', 'label'] = 'r2l'

# Probe attacks
full.loc[full.label == 'satan', 'label'] = 'probe'
full.loc[full.label == 'ipsweep', 'label'] = 'probe'
full.loc[full.label == 'nmap', 'label'] = 'probe'
full.loc[full.label == 'portsweep', 'label'] = 'probe'
full.loc[full.label == 'saint', 'label'] = 'probe'
full.loc[full.label == 'mscan', 'label'] = 'probe'

# Make this a binary classification task instead
#df.label[df.label != 'normal'] = 'attack'
#full.label[full.label != 'normal'] = 'attack'

full2 = pd.get_dummies(full, drop_first=False)

features = list(full2.columns[:-5])

y_train = full2[0:df.shape[0]][['label_normal', 'label_dos', 'label_probe', 'label_r2l', 'label_u2r']]
X_train = full2[0:df.shape[0]][features]
y_test = full2[df.shape[0]:][['label_normal', 'label_dos', 'label_probe', 'label_r2l', 'label_u2r']]
X_test = full2[df.shape[0]:][features]

scaler = MinMaxScaler().fit(X_train)
X_train_scaled = scaler.transform(X_train)
X_test_scaled = scaler.transform(X_test)

print(X_train_scaled.shape, y_train.shape)
print(X_test_scaled.shape, y_test.shape)


model = Sequential()
model.add(Dense(256, activation='relu', input_shape=(122,)))
model.add(Dropout(0.5))
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
#model.add(Dense(128, activation='relu'))
#model.add(Dropout(0.6))
model.add(Dense(5, activation='softmax'))

#model.summary()

model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

x = tf.placeholder(tf.float32, shape=(None, 122))
y = tf.placeholder(tf.float32, shape=(None, 5))


sess = tf.Session()
predictions = model(x)
init = tf.global_variables_initializer()
sess.run(init)


def evaluate():
  eval_params = {'batch_size': FLAGS.batch_size}
  accuracy = model_eval(sess, x, y, predictions, X_test_scaled, y_test, args=eval_params)
  print('Test accuracy on legitimate test examples: ' + str(accuracy))


train_params = {
        'nb_epochs': FLAGS.nb_epochs,
        'batch_size': FLAGS.batch_size,
        'learning_rate': FLAGS.learning_rate
    }
model_train(sess, x, y, predictions, X_train_scaled, y_train, evaluate=evaluate, args=train_params)

# Fast gradient sign method
# adv_x = fgsm(x, predictions, eps=0.3)
# eval_params = {'batch_size': FLAGS.batch_size}
# X_test_adv, = batch_eval(sess, [x], [adv_x], [X_test_scaled], args=eval_params)

# accuracy = model_eval(sess, x, y, predictions, X_test_adv, y_test, args=eval_params)
# print('Test accuracy on adversarial examples: ' + str(accuracy))
# print("Repeating the process, using adversarial training")

source_samples = X_test_scaled.shape[0]
# Jacobian-based Saliency Map
results = np.zeros((FLAGS.nb_classes, FLAGS.source_samples), dtype='i')
perturbations = np.zeros((FLAGS.nb_classes, FLAGS.source_samples), dtype='f')
grads = jacobian_graph(predictions, x, FLAGS.nb_classes)
X_adv = np.zeros((FLAGS.source_samples, X_test_scaled.shape[1]))

X_test_scaled1 = np.array(X_test_scaled)
y_test1 = np.array(y_test)


for sample_ind in range(0, FLAGS.source_samples):
    # We want to find an adversarial example for each possible target class
    # (i.e. all classes that differ from the label given in the dataset)
    current_class = int(np.argmax(y_test1[sample_ind]))
    target_classes = other_classes(FLAGS.nb_classes, current_class)
    
    # for target in target_classes:
    for target in [0]:
        print('--------------------------------------')
        print('Creating adv. example for target class ' + str(target))
        if current_class == 0:
          break

        # This call runs the Jacobian-based saliency map approach
        adv_x, res, percent_perturb = jsma(sess, x, predictions, grads,
                                               X_test_scaled1[sample_ind: (sample_ind+1)],
                                               target, theta=1, gamma=0.1,
                                               increase=True, back='tf',
                                               clip_min=0, clip_max=1)
        X_adv[sample_ind] = adv_x - X_test_scaled1[sample_ind: (sample_ind+1)]
        ind = np.where(X_adv != 0)[1]
        print(ind, X_test.columns[ind], current_class)

        # Update the arrays for later analysis
        results[target, sample_ind] = res
        perturbations[target, sample_ind] = percent_perturb


# Compute the number of adversarial examples that were successfuly found
# nb_targets_tried = ((FLAGS.nb_classes - 1) * FLAGS.source_samples)
nb_targets_tried = FLAGS.source_samples
succ_rate = float(np.sum(results)) / nb_targets_tried
print('Avg. rate of successful adv. examples {0:.2f}'.format(succ_rate))

# Compute the average distortion introduced by the algorithm
percent_perturbed = np.mean(perturbations[0, :])
print('Avg. rate of perturbed features {0:.2f}'.format(percent_perturbed))

# Compute the average distortion introduced for successful samples only
percent_perturb_succ = np.mean(perturbations[0, :] * (results == 1))
print('Avg. rate of perturbed features for successful '
         'adversarial examples {0:.2f}'.format(percent_perturb_succ))

# Close TF session
sess.close()



